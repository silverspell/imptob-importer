# -*- coding: utf-8 -*-
from sqlalchemy.sql.functions import current_date

__author__ = 'Cem'


import logging
import sys
import os
import MySQLdb as mdb
from xml.etree import ElementTree
import smtplib
import time
from datetime import date, timedelta


class Customer():
	def __init__(self, route_group_id, tl_name, route_id, route_name, customer_id, code, commercial_title, is_active):
		self.route_group_id = int(route_group_id)
		self.tl_name = tl_name
		self.route_id = int(route_id)
		self.route_name = route_name
		self.customer_id = int(customer_id)
		self.code = str(code)
		self.commercial_title = commercial_title
		self.is_active = int(is_active)

	def __repr__(self):
		return self.commercial_title

	def update_or_insert(self, db):
		cur = db.cursor()
		cur.execute("select count(id) as cnt from customers where customer_id = %d" % (self.customer_id))
		ret = ""
		if cur.fetchone()[0] == 1:
			ret = "update"
			#logging.debug("update %s", self.code)
			sql = "update customers set route_group_id = %d, tl_name = '%s', route_id = %d, route_name = '%s', code = '%s', commercial_title = '%s', is_active = %d where customer_id = %d" %(self.route_group_id, self.tl_name, self.route_id, self.route_name, self.code, self.commercial_title, self.is_active, self.customer_id)

		else:
			ret = "insert"
			#logging.debug("insert %s", self.code)
			sql = "insert into customers (customer_id, route_group_id, tl_name, route_id, route_name, code, commercial_title, is_active) values(%d, %d, '%s', %d, '%s', '%s', '%s', %d)" %(
				self.customer_id, self.route_group_id, self.tl_name, self.route_id, self.route_name, self.code, self.commercial_title, self.is_active)

		cur.execute(sql)
		return ret

class AppUser():
	def __init__(self, tl_name, username, is_active):
		self.tl_name = tl_name
		self.username = username
		self.is_active = is_active

	def update_or_insert(self, db):
		cur = db.cursor()
		cur.execute("select count(id) as cnt from app_users where username = %s" % self.username)
		ret = ""
		if cur.fetchone()[0] == 1:
			ret = "update"
			sql = "update app_users set tl_name = '{0}', is_active = {1}, edit_date = NOW() where username = '{2}'".format(self.tl_name, self.is_active, self.username)
		else:
			ret = "insert"
			sql = "insert into app_users (tl_name, username, is_active, create_date) values ('{0}', '{1}', {2}, 'NOW()')".format(self.tl_name, self.username, self.is_active)

		cur.execute(sql)
		return ret

def import_customers(db):
	filename = "customer.xml"
	if not os.path.exists(filename):
		return 0,0
	inserts = 0
	updates = 0
	has_errors = False
	document = ElementTree.parse(filename)

	for customer in document.findall("customer"):
		code = customer.find("code").text
		route_group_id = customer.find("route_group_id").text
		tl_name = customer.find("tl_name").text
		route_id = customer.find("route_id").text
		route_name = customer.find("route_name").text
		customer_id = customer.find("customer_id").text
		commercial_title = customer.find("commercial_title").text
		is_active = customer.find("is_active").text

		c = Customer(route_group_id, tl_name, route_id, route_name, customer_id, code, commercial_title, is_active)
		try:
			op = c.update_or_insert(db)
			if op == "update" :
				updates += 1
			else:
				inserts += 1
		except Exception, e:
			logging.error("%s".format(str(e)))
			has_errors = True
			send_email("Import error: customer", code)
			logging.error("end import with error")
			break


	if has_errors:
		db.rollback()
		return 0, 0
	else:
		db.commit()
	return inserts, updates

def import_users(db):
	filename = "user.xml"
	if not os.path.exists(filename):
		return 0,0
	inserts = 0
	updates = 0
	has_errors = False
	document = ElementTree.parse(filename)

	for user in document.findall("user"):
		tl_name = user.find("tl_name").text
		is_active = user.find("is_active").text
		username = user.find("username").text

		u = AppUser(tl_name, username, is_active)
		try:
			op = u.update_or_insert(db)
			if op == "update":
				updates += 1
			else:
				inserts += 1
		except Exception, e:
			logging.error("{0}".format(str(e)))
			has_errors = True
			send_email("Import error: user", username)
			logging.error("end import with error (user)")
			break
	if has_errors:
		db.rollback()
		return 0, 0
	else:
		db.commit()
	return inserts, updates

def visit_reports(db):
	yesterday = date.today() - timedelta(1)

	sql = """
	select c.tl_name, c.customer_id, Date(v.visit_time) as visit_time, v.id
	from customers c
	join visits v on c.id = v.customer_id
	where
	Date(v.visit_time) = '{0}'
	""".format(yesterday.strftime("%Y-%m-%d"))
	cur = db.cursor(mdb.cursors.DictCursor)
	cur.execute(sql)
	visits = cur.fetchall()
	xml = ""
	for visit in visits:
		tl_name = visit["tl_name"]
		customer_id = visit["customer_id"]
		visit_time = visit["visit_time"]
		visit_id = visit["id"]

		sql_detail = """
		select
			products.imp_id as product_id,
			case when out_of_stock_reasons.imp_id is null then 0 else out_of_stock_reasons.imp_id end as out_of_stock_reason_id,
			visit_products.product_exists
		from
			visit_products
			left join products on products.id = visit_products.product_id
			left outer join out_of_stock_reasons on out_of_stock_reasons.id = visit_products.out_of_stock_reason
		where
			visit_id = {0}
		""".format(visit_id)
		cur_detail = db.cursor(mdb.cursors.DictCursor)
		cur_detail.execute(sql_detail)
		visit_details = cur_detail.fetchall()

		detail_xml = ""

		for vd in visit_details:
			product_id = vd["product_id"]
			oosr_id = vd["out_of_stock_reason_id"]
			product_exists = vd["product_exists"]
			detail_xml += """<detail><product_id>{0}</product_id><reason>{1}</reason><exists>{2}</exists></detail>
			""".format(product_id, oosr_id, product_exists)

		visit_xml = """<visit>
			<tl_name>{0}</tl_name><customer_id>{1}</customer_id><visit_time>{2}</visit_time>
			<details>
				{3}
			</details>
		</visit>
		""".format(tl_name, customer_id, visit_time, detail_xml)
		xml += visit_xml
	return """<visits>{0}
			</visits>""".format(xml)

def main():
	logging.basicConfig(filename='importer.log', level=logging.DEBUG)
	logging.info("Start import")
	start_time = time.time()
	#db = mdb.connect("localhost", "imptobdb", "a_f7D32#&jFy-uiV", "imptob")
	db = mdb.connect("localhost", "root", "", "imptob")
	report_xml = visit_reports(db)
	f = open("report.xml", "w")
	f.write(report_xml)
	f.close()
	customer_result = import_customers(db)
	user_result = import_users(db)

	end_time = time.time()
	db.close()
	report(start_time, end_time, customer_result, user_result)
	logging.info("Import complete")

def report(start_time, end_time, customer_result, user_result):
	total_time = int(end_time - start_time)
	body = "Customer inserted: {0}\nCustomer updated {1}\nUser inserted: {2}\nUsers updated: {3}\nTotal time (sec): {4}".format(customer_result[0], customer_result[1], user_result[0], user_result[1], total_time)
	send_email("Import results", body)

def send_email(subject, body):
	to = "notifications@kuryaz.com"
	p = "V6AZJl73XG4V"
	s = smtplib.SMTP("smtp.yandex.com.tr", 587)
	s.ehlo()
	s.starttls()
	s.login(to, p)
	header = "To:{0}\nFrom:{1}\nSubject:{2}  \n".format(to, to, subject)
	msg = "{0}\n {1}\n\n".format(header, body)
	s.sendmail(to, to, msg)
	s.close()

if __name__ == "__main__":
	main()