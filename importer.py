'''
Created on 30 Eki 2013

@author: Cem
'''
# -*- coding: utf-8 -*-

import logging
import sys
import MySQLdb as mdb
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer
from pyftpdlib.servers import ThreadedFTPServer
from pyftpdlib.authorizers import DummyAuthorizer
from xml.etree import ElementTree
import smtplib
import time


class ImportHandler(FTPHandler):
    timeout = 0
    def on_connect(self):
        print "{0}:{1} connected".format(self.remote_ip, self.remote_port)
        
    def on_disconnect(self):
        pass
    
    def on_login(self, username):
        pass
    
    def on_logout(self, username):
        pass
    
    def on_file_sent(self, file):
        pass
    
    def on_file_received(self, file):
        logging.info("Received {0}".format(file))
        #prepare_file(file)
    
    def on_incomplete_file_sent(self, file):
        pass
    
    def on_incomplete_file_received(self, file):
        import os
        os.remove(file)
        send_error("file sent is incomplete")
     
class Customer():
    def __init__(self, route_group_id, tl_name, route_id, route_name, customer_id, code, commercial_title, is_active):
        self.route_group_id = int(route_group_id)
        self.tl_name = tl_name
        self.route_id = int(route_id)
        self.route_name = route_name
        self.customer_id = int(customer_id)
        self.code = str(code)
        self.commercial_title = commercial_title
        self.is_active = int(is_active)

    def __repr__(self):
        return self.commercial_title
    
    def update_or_insert(self, db):
        cur = db.cursor()
        cur.execute("select count(id) as cnt from customers where customer_id = %d" % (self.customer_id))
        ret = ""
        if cur.fetchone()[0] == 1:
            ret = "update"
            #logging.debug("update %s", self.code)
            sql = "update customers set route_group_id = %d, tl_name = '%s', route_id = %d, route_name = '%s', code = '%s', commercial_title = '%s', is_active = %d where customer_id = %d" %(self.route_group_id, self.tl_name, self.route_id, self.route_name, self.code, self.commercial_title, self.is_active, self.customer_id)
            
        else:
            ret = "insert"
            #logging.debug("insert %s", self.code)
            sql = "insert into customers (customer_id, route_group_id, tl_name, route_id, route_name, code, commercial_title, is_active) values(%d, %d, '%s', %d, '%s', '%s', '%s', %d)" %(
                self.customer_id, self.route_group_id, self.tl_name, self.route_id, self.route_name, self.code, self.commercial_title, self.is_active)

        cur.execute(sql)
        return ret
    
            

def prepare_file(file):
    logging.info("Started importing")
    start_time = time.time()
    db = mdb.connect("localhost", "imptobdb", "a_f7D32#&jFy-uiV", "imptob")
    has_errors = False
    document = ElementTree.parse(file)
    inserts = 0
    updates = 0
    db.cursor().execute("update customers set is_active=0")
	
    for customer in document.findall("customer"):
        code = customer.find("code").text
        route_group_id = customer.find("route_group_id").text
        tl_name = customer.find("tl_name").text
        route_id = customer.find("route_id").text
        route_name = customer.find("route_name").text
        customer_id = customer.find("customer_id").text
        commercial_title = customer.find("commercial_title").text
        is_active = customer.find("is_active").text
        
        c = Customer(route_group_id, tl_name, route_id, route_name, customer_id, code, commercial_title, is_active)
        try:
            op = c.update_or_insert(db)
            if op == "update" :
                updates = updates + 1
            else:
                inserts = inserts + 1
                
        except Exception, e:
            logging.error("%s".format(str(e)))
            has_errors = True
            send_error(repr(c))
            logging.error("end import with error")
            break
            
        
    if has_errors:
        db.rollback()
    else:        
        db.commit()
    
    end_time = time.time()
    db.close()
    report(start_time, end_time, inserts, updates)
    logging.info("Ended import")
    
def report(start_time, end_time, inserts, updates):
    total_time = int(end_time - start_time)
    body = "Items inserted: {0}\nItems updated: {1}\nTotal time (sec): {2}".format(inserts, updates, total_time)
    send_error(body)    
    
def send_error(customer):
    to = "notifications@kuryaz.com"
    p = "V6AZJl73XG4V"
    s = smtplib.SMTP("smtp.yandex.com.tr", 587)
    s.ehlo()
    s.starttls()
    s.login(to, p)
    header = "To:{0}\nFrom:{1}\nSubject:Import Error \n".format(to, to)
    msg = "{0}\nProblem in import: {1}\n\n".format(header, customer)
    s.sendmail(to, to, msg)
    s.close()
            
        
def main():
    logging.basicConfig(filename='pyftpd.log', level=logging.DEBUG)
    authorizer = DummyAuthorizer()
    authorizer.add_user("imptob", "sg7QfL5J", homedir=".", perm = "elradfmw")
    
    handler = ImportHandler
    handler.authorizer = authorizer
    handler.passive_ports = range(45130, 45137)
    #handler.tcp_no_delay = True
    handler.use_sendfile = True
    handler.banner = "FTP Ready"
    handler.log_prefix = "XXX [%(username)s]@%(remote_ip)s"
    server = FTPServer(("0.0.0.0", 21), handler)
    #server = new ThreadedFTPServer(("0.0.0.0", 21), handler)
    server.serve_forever()
    
if __name__ == "__main__":
    main()
